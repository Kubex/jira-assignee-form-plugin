package ut.jjanczyk.jira.plugin.assigneeform;

import com.atlassian.jira.mock.servlet.MockHttpServletRequest;
import com.atlassian.jira.mock.servlet.MockHttpServletResponse;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;
import jjanczyk.jira.plugin.assigneeform.AssigneeFormServlet;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.ServletException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

/**
 * Created by Jakub on 2015-08-05.
 */
public class AssigneeFormServletTest {
    private AssigneeFormServlet assigneeFormServlet;

    @Mock
    private UserManager userManager;

    @Mock
    private LoginUriProvider loginUriProvider;

    @Mock
    private TemplateRenderer templateRenderer;

    private MockHttpServletRequest request;
    private MockHttpServletResponse response;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        assigneeFormServlet = new AssigneeFormServlet(userManager, loginUriProvider, templateRenderer);

        request = new MockHttpServletRequest();
        request.setMethod("GET");
        request.setRequestURL("/assignee-form");

        response = new MockHttpServletResponse();
    }

    @Test
    public void testNotAuthenticated() throws ServletException, IOException, URISyntaxException {
        when(userManager.getRemoteUserKey(request)).thenReturn(null);
        URI returnUri = new URI(request.getRequestURI());
        URI loginUri = new URI("login-uri");
        when(loginUriProvider.getLoginUri(returnUri)).thenReturn(loginUri);

        assigneeFormServlet.service(request, response);

        assertEquals(loginUri.getPath(), response.getRedirect());

        verify(userManager, times(1)).getRemoteUserKey(request);
        verify(loginUriProvider, times(1)).getLoginUri(returnUri);
        verifyNoMoreInteractions(userManager, loginUriProvider);
        verifyZeroInteractions(templateRenderer);
    }

    @Test
    public void testCorrectContentTypeSet() throws ServletException, IOException {
        when(userManager.getRemoteUserKey(request)).thenReturn(new UserKey("testuserkey"));

        assigneeFormServlet.service(request, response);

        assertEquals("text/html;charset=utf-8", response.getContentType());

        verify(userManager, times(1)).getRemoteUserKey(request);
        verifyNoMoreInteractions(userManager);
        verifyZeroInteractions(loginUriProvider);
    }

    @Test
    public void testCorrectTemplateRendered() throws ServletException, IOException {
        when(userManager.getRemoteUserKey(request)).thenReturn(new UserKey("testuserkey"));

        assigneeFormServlet.service(request, response);

        verify(templateRenderer, times(1)).render("assignee-form.vm", response.getWriter());
        verifyNoMoreInteractions(templateRenderer);
        verifyZeroInteractions(loginUriProvider);
    }
}
