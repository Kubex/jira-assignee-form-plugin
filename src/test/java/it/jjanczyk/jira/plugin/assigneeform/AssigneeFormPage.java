package it.jjanczyk.jira.plugin.assigneeform;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.TimedElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

/**
 * Helper class for UI tests
 * <p>
 * Created by Jakub on 2015-08-05.
 */
public class AssigneeFormPage extends AbstractJiraPage {

    @ElementBy(id = "check-button")
    private PageElement checkButton;
    @ElementBy(id = "issue-name")
    private PageElement issueNameInput;
    @ElementBy(id = "check-result")
    private PageElement checkResultSection;
    @ElementBy(id = "check-result-name")
    private PageElement checkResultName;
    @ElementBy(id = "no-issue-group")
    private PageElement noIssueSection;

    @Override
    public TimedCondition isAt() {
        return this.checkButton.timed().isPresent();
    }

    @Override
    public String getUrl() {
        return "/plugins/servlet/assignee-form";
    }

    public AssigneeFormPage check() {
        this.checkButton.click();
        return pageBinder.bind(AssigneeFormPage.class);
    }

    public void setIssueName(String input) {
        this.issueNameInput.type(input);
    }

    public TimedElement getCheckResultSection() {
        return checkResultSection.timed();
    }

    public TimedElement getCheckResultName() {
        return checkResultName.timed();
    }

    public TimedElement getNoIssueSection() {
        return noIssueSection.timed();
    }
}
