package it.jjanczyk.jira.plugin.assigneeform;

import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.jira.testkit.client.util.TimeBombLicence;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.elements.query.Poller;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertFalse;

/**
 * Class for simple UI tests
 * <p>
 * Created by Jakub on 2015-08-05.
 */
public class AssigneeFormPageTest {
    private JiraTestedProduct jira;

    private String issue1Key;
    private String issue2Key;

    private String userNameAndPassword = "user1";
    private String adminName = "Administrator";

    @Before
    public void setUp() {
        jira = TestedProductFactory.create(JiraTestedProduct.class);
        Backdoor backdoor = new Backdoor(new TestKitLocalEnvironmentData());
        backdoor.restoreBlankInstance(TimeBombLicence.LICENCE_FOR_TESTING);
        String projectKey = "TEST";
        backdoor.generalConfiguration().allowUnassignedIssues();
        backdoor.project().addProject("Test Project", projectKey, "admin");
        issue1Key = backdoor.issues().createIssue(projectKey, "desc", "admin").key();

        backdoor.usersAndGroups().addUser(userNameAndPassword, userNameAndPassword, userNameAndPassword, "user1@example.com");
        backdoor.usersAndGroups().addUserToGroup(userNameAndPassword, "jira-users");
        backdoor.usersAndGroups().addUserToGroup(userNameAndPassword, "jira-developers");

        issue2Key = backdoor.issues().createIssue(projectKey, "desc", userNameAndPassword).key();
    }

    @Test
    public void findingIssueSetsProperAssigneeAsAdmin() {
        AssigneeFormPage page = jira.gotoLoginPage().loginAsSysAdmin(AssigneeFormPage.class);

        page.setIssueName(issue1Key);
        page.check();

        Poller.waitUntil(page.getCheckResultName().getText(), equalTo(adminName));
    }

    @Test
    public void findingIssueSetsProperAssigneeAsUser1() {
        AssigneeFormPage page = jira.gotoLoginPage().loginAsSysAdmin(AssigneeFormPage.class);

        page.setIssueName(issue2Key);
        page.check();

        Poller.waitUntil(page.getCheckResultName().getText(), equalTo(userNameAndPassword));
    }

    @Test
    public void findingIssueAsRegularUserSetsProperAssigneeAsAdmin() {
        AssigneeFormPage page = jira.gotoLoginPage().login(userNameAndPassword, userNameAndPassword, AssigneeFormPage.class);

        page.setIssueName(issue1Key);
        page.check();

        Poller.waitUntil(page.getCheckResultName().getText(), equalTo(adminName));
    }

    @Test
    public void noIssueSectionVisible() {
        AssigneeFormPage page = jira.gotoLoginPage().loginAsSysAdmin(AssigneeFormPage.class);

        page.setIssueName("noIssueName");
        page.check();

        Poller.waitUntilTrue(page.getNoIssueSection().isVisible());
        assertFalse(page.getCheckResultSection().isVisible().now());
    }
}
