package jjanczyk.jira.plugin.assigneeform;

import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.templaterenderer.TemplateRenderer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;

/**
 * Servlet for serving Assignee Form Velocity template
 *
 * Created by Jakub on 2015-08-05.
 */
public class AssigneeFormServlet extends HttpServlet {

    private final UserManager userManager;
    private final LoginUriProvider loginUriProvider;
    private final TemplateRenderer templateRenderer;

    public AssigneeFormServlet(UserManager userManager, LoginUriProvider loginUriProvider, TemplateRenderer templateRenderer) {
        this.userManager = userManager;
        this.loginUriProvider = loginUriProvider;
        this.templateRenderer = templateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        UserKey userKey = userManager.getRemoteUserKey(req);

        if (userKey == null) {
            redirectToLogin(req, resp);
            return;
        }

        resp.setContentType("text/html;charset=utf-8");
        templateRenderer.render("assignee-form.vm", resp.getWriter());
    }

    private void redirectToLogin(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.sendRedirect(loginUriProvider.getLoginUri(getReturnUri(req)).toASCIIString());
    }

    private URI getReturnUri(HttpServletRequest req) {
        StringBuffer builder = req.getRequestURL();
        String queryString = req.getQueryString();
        if (queryString != null && !queryString.isEmpty())
        {
            builder.append("?");
            builder.append(queryString);
        }
        return URI.create(builder.toString());
    }
}
