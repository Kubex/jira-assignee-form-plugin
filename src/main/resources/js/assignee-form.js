/**
 * Function to attach submit action to the form. It is calling REST API to find issue and its assignee.
 */
AJS.toInit(function () {
    'use strict';

    var issueApiUrl = AJS.contextPath() + '/rest/api/2/issue/',
        assigneeQuery = '?fields=assignee';

    hideResultSections();

    AJS.$('#assignee-form').submit(checkForAssignee);

    function hideResultSections() {
        AJS.$('#result-group').hide();
        AJS.$('#no-issue-group').hide();
    }

    function checkForAssignee(event) {
        var issueKey = AJS.$('#issue-name').val(),
            fullUrl = issueApiUrl + issueKey + assigneeQuery;

        event.preventDefault();

        AJS.$.ajax({
            url: fullUrl,
            dataType: 'json'
        }).done(displayAssignee)
            .fail(displayErrorMessage);

        function displayAssignee(data) {
            var dataToInsert = getDataToInsertInHtml(data);

            fillHtmlData(dataToInsert);

            AJS.$('#no-issue-group').hide();
            AJS.$('#result-group').show();
        }

        function displayErrorMessage(xhr) {
            if (xhr.status === 404) {
                AJS.$('#result-group').hide();
                AJS.$('#no-issue-group').show();
            }
        }

        function getDataToInsertInHtml(data) {
            var assignee = data.fields.assignee,
                name, avatarUrl;

            if (assignee === null) {
                name = 'Unassigned';
                avatarUrl = 'http://localhost:2990/jira/secure/useravatar?size=xsmall&avatarId=10123';
            } else {
                name = data.fields.assignee.displayName;
                avatarUrl = data.fields.assignee.avatarUrls['16x16'];
            }
            return {name: name, avatarUrl: avatarUrl};
        }

        function fillHtmlData(dataToInsert) {
            AJS.$('#check-result-name').text(dataToInsert.name);
            AJS.$('#avatar').attr('src', dataToInsert.avatarUrl);
            AJS.$('#issue-link').attr('href', AJS.contextPath() + '/browse/' + issueKey);
        }
    }
});